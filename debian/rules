#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

export QT_SELECT := qt5

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

BUILDDIR_BASE = $(CURDIR)/obj-$(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)

DEB_HOST_ARCH = $(shell dpkg-architecture -qDEB_HOST_ARCH)
BUILDDIR_SUFFIX_i386 = sse2
# VERY unhappy about the last one
CONFIGURE_OPTIONS_sse2 = "QMAKE_CFLAGS+=-mmmx -msse -msse2" "QMAKE_CXXFLAGS+= -mmmx -msse -msse2" "QT.global_private.enabled_features+=sse2"

%:
	dh $@ --with pkgkde_symbolshelper

override_dh_auto_clean:
	$(foreach SUFFIX, ${BUILDDIR_SUFFIX_${DEB_HOST_ARCH}} -, \
		dh_auto_clean -B${BUILDDIR_BASE}$(patsubst %--,,-${SUFFIX}); \
	)
	rm -rf $(CURDIR)/test_root
	rm -rf $(CURDIR)/.local

override_dh_auto_configure:
	set -e; $(foreach SUFFIX, ${BUILDDIR_SUFFIX_${DEB_HOST_ARCH}} -, \
		dh_auto_configure  -B${BUILDDIR_BASE}$(patsubst %--,,-${SUFFIX}) -- QT_BUILD_PARTS+=tests QTREPOS+=$(BUILDDIR_BASE) QTREPOS+=/usr/share/qt5 QMAKE_PYTHON=python3 $(CURDIR)/qtdeclarative.pro ${CONFIGURE_OPTIONS_$(patsubst %-,,${SUFFIX})}; \
	)

override_dh_auto_build-indep:
	dh_auto_build -B$(BUILDDIR_BASE) -- docs

override_dh_auto_build-arch:
	# Makefiles need to be regenerated after qmltypes.prf becomes available.
	# (The below line seems to be not needed since the bare existence of override
	# target makes dh(1) change the build order, but let's keep it to be safe.)
	#	find $(BUILDDIR_BASE) -name Makefile -and -not -path $(BUILDDIR_BASE)/Makefile -delete
	set -e; $(foreach SUFFIX, ${BUILDDIR_SUFFIX_${DEB_HOST_ARCH}} -, \
		dh_auto_build -B${BUILDDIR_BASE}$(patsubst %--,,-${SUFFIX}); \
	)

override_dh_auto_install-arch:
	set -e; $(foreach SUFFIX, ${BUILDDIR_SUFFIX_${DEB_HOST_ARCH}} -, \
		dh_auto_build -B${BUILDDIR_BASE}$(patsubst %--,,-${SUFFIX}) -- INSTALL_ROOT=$(CURDIR)/debian/tmp/$(patsubst %-,,${SUFFIX}) install; \
	)

	# Remove libtool-like files
	rm -f debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/*.la

	# Reproducible builds: remove build paths from prl files.
	sed -i -e '/^QMAKE_PRL_BUILD_DIR/d' debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libQt5*.prl

	# Fix wrong permissions in examples.
	find $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/qt5/ -type f \( \
	-name '*.png' \
	-o -name '*.php' \
	-o -name '*.xsl' \
	-o -name '*.xml' \
	-o -name '*.js' \
	-o -name '*.jpg' \
	-o -name '*.qml' \
	\) -print0 | xargs -0 chmod a-x

override_dh_auto_install-indep:
	dh_auto_build -B$(BUILDDIR_BASE) -- INSTALL_ROOT=$(CURDIR)/debian/tmp install_docs

override_dh_link:
	dh_link
	ls debian/qtdeclarative5-dev-tools/usr/lib/qt5/bin | xargs -t -I {} \
		dh_link -pqtdeclarative5-dev-tools usr/lib/qt5/bin/{} usr/lib/$(DEB_HOST_MULTIARCH)/qt5/bin/{}
	dh_link -pqmlscene usr/lib/qt5/bin/qmlscene usr/lib/$(DEB_HOST_MULTIARCH)/qt5/bin/qmlscene
	dh_link -pqml usr/lib/qt5/bin/qml usr/lib/$(DEB_HOST_MULTIARCH)/qt5/bin/qml

override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
ifneq (0,$(shell which cmake > /dev/null 2>&1)$(.SHELLSTATUS))
	# these tests fail when cmake is installed
	dh_auto_build -B$(BUILDDIR_BASE) -- install INSTALL_ROOT=$(CURDIR)/test_root
	mkdir -p $(CURDIR)/.local/share/QtProject/tst_qqmlengine
	# - hppa: QML engine is broken, see bug #973659
	# - ia64: QML engine is broken, see bug #929682
	# - mips*: Some tests are failing because of bug #868745
	# - powerpc, ppc64, s390x: item-grabber test fails on big endian, see https://bugreports.qt.io/browse/QTBUG-56806
	# - sparc64: QML engine is broken, see bug #894726
ifneq (,$(filter $(DEB_HOST_ARCH),hppa ia64 mips mips64el mipsel powerpc ppc64 s390x sparc64))
	-xvfb-run -a \
	         -s "-screen 0 1024x768x24 +extension RANDR +extension RENDER +extension GLX" \
	         dh_auto_test -B$(BUILDDIR_BASE) --max-parallel=1 -- -k -Ctests/auto \
	         QML2_IMPORT_PATH=$(CURDIR)/test_root/usr/lib/$(DEB_HOST_MULTIARCH)/qt5/qml \
	         QT_PLUGIN_PATH=$(BUILDDIR_BASE)/plugins \
	         LD_LIBRARY_PATH=$(BUILDDIR_BASE)/lib
else
	xvfb-run -a \
	         -s "-screen 0 1024x768x24 +extension RANDR +extension RENDER +extension GLX" \
	         dh_auto_test -B$(BUILDDIR_BASE) --max-parallel=1 -- -k -Ctests/auto \
	         QML2_IMPORT_PATH=$(CURDIR)/test_root/usr/lib/$(DEB_HOST_MULTIARCH)/qt5/qml \
	         QT_PLUGIN_PATH=$(BUILDDIR_BASE)/plugins \
	         LD_LIBRARY_PATH=$(BUILDDIR_BASE)/lib
endif
endif
endif

override_dh_auto_test-indep:
